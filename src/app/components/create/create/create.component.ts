import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service'
import { Sprint } from '../../../models/sprint.model'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router'

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent {
  postSprint: FormGroup;


  constructor(private apiService: ApiService, private fb: FormBuilder, private router: Router) {
    this.postSprint = this.fb.group({
      'name': '',
      'progress': '',
      'description': '',
      'notify': '',
      'user': 'auth0|5ba130406a52865dc2e6622f'
    })
  }


  post(postSprint) {

    var d = new Date();
    postSprint.createdAt = d.toISOString();

    if (postSprint.name == 'Instant (5s)') {
      postSprint.duration = 5;
      var n = new Date(d.getTime() + (5 * 1000));
    }
    else if (postSprint.name == 'Very short (5min)') {
      postSprint.duration = 5 * 60;
      var n = new Date(d.getTime() + (5 * 60 * 1000));
    }
    else if (postSprint.name == 'Shot (15min)') {
      postSprint.duration = 15 * 60;
      var n = new Date(d.getTime() + (15 * 60 * 1000));
    }
    else if (postSprint.name == 'Pomodoro (25min)') {
      postSprint.duration = 25 * 60;
      var n = new Date(d.getTime() + (25 * 60 * 1000));
    }
    else if (postSprint.name == 'Long (45min)') {
      postSprint.duration = 45 * 60;
      var n = new Date(d.getTime() + (45 * 60 * 1000));
    }
    else if (postSprint.name == 'Very long (60min)') {
      postSprint.duration = 60 * 60;
      var n = new Date(d.getTime() + (60 * 60 * 1000));
    }

    postSprint.finishedAt = n.toISOString();

    postSprint.status = "Completed";
    postSprint.progress = 100;    


    this.apiService.postSprint(postSprint);
    console.log("create.component " + postSprint);
  }

}
