import{HttpClient}from '@angular/common/http'
import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { logging } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class ApiService {    
  sprints =[]  
  path='http://localhost:3000'

  constructor(private http: HttpClient) { }
  

  getSprints() {
      this.http.get<any>(this.path + '/sprints' ).subscribe(res => {
          this.sprints = res
      })
  }

  postSprint(sprint) {
    this.http.post(this.path + '/sprints/add', sprint).subscribe(res => {      
  });
    console.log("api.service " + sprint);    
}

}
