import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home/home.component';
import { SprintsComponent } from './components/sprints/sprints/sprints.component';
import { CreateComponent } from './components/create/create/create.component';
import { ProgressComponent } from './components/progress/progress/progress.component';
import { ApiService } from './services/api.service';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from "@angular/forms";
import { AuthService } from './auth.service';




const routes = [
  { path: 'sprints', component: SprintsComponent },
  { path: '', component: HomeComponent },
  { path: 'create', component: CreateComponent },
  { path: 'progress', component: ProgressComponent }
]


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SprintsComponent,
    CreateComponent,
    ProgressComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [ApiService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
