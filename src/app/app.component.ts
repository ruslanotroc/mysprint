import { Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mysprint';

  constructor(private auth: AuthService) {
    this.auth.handleAuthentication();
  }

  login() {
    console.log("app.component.login");
    this.auth.login()
  }

  logout() {
    console.log("app.component.logout");
    this.auth.logout()
  }

  goToLogout() {
    window.location.href = 'https://otr.auth0.com/v2/logout?returnTo=http%3A%2F%2Flocalhost:4200&client_id=kQsBVAYxOil6o0p68jwvGuHnS0hWdPPs';
  }

}
