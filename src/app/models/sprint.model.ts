export interface Sprint {
    name: String,
    duration: String,
    status: String,
    progress: String,
    description: String,
    notify: String,
    user: String,
    createdAt: String,
    startedAt: String,
    finishedAt: String
}