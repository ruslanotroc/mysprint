const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const cors = require('cors');

const app = express();

const api = require('./routes/route');



app.use(bodyParser.json());


app.use(cors());


app.use('/', api);


const port = process.env.PORT || '3000';
app.set('port', port);


app.listen(port, () => {
    console.log(`server running on localhost:${port}`)
})
