const express = require('express');
const MongoClient = require('mongoDb').MongoClient;
const assert = require('assert');


const router = express.Router();
const uri = ('mongodb://localhost:27017/sprintDb');


//Get sprints stored in the database

router.route('/sprints').get((req,res) =>{
    console.log("get((req,res)");
    MongoClient.connect(uri, {useNewUrlParser: true}, (err,client) =>{
        assert.equal(null, err);
        var db = client.db('sprintDb');
        db.collection("sprints").find({}).toArray((err,sprints)=>{
            assert.equal(null,err);
            res.json(sprints);
            client.close();
        });
    });
});


//Post sprint to the database

router.route('/sprints/add').post((req,res) =>{
    console.log("post((req,res)");
    MongoClient.connect(uri, {useNewUrlParser: true}, (err,client) =>{        
        var db = client.db('sprintDb');
        assert.equal(null, err);
        db.collection("sprints").insertOne(req.body);
        
        res.send('Data received:\n' + JSON.stringify(req.body));
        client.close(); 
    });
});


module.exports=router;
